EMI_Gloves
==========

 

Projet de gants interactifs pour Jacopo Baboni, de l’EMI.  


\+ switch + batteries

 

### Microcontrôleur

Wemos ESP8266

 

### Capteur de distance

VL53L0X

 

### Accelerometre

MPU 6050

Code basé sur l'exemple fourni dans
[https://github.com/jrowberg/i2cdevlib](https://github.com/jrowberg/i2cdevlib)

Besoin de tweaker la librairie i2c selon les infos trouvées ici:
[https://github.com/halfcadence/i2cdevlib/commit/94677116c62aff6a2ce08d96522d3a8e89d12b4b](https://github.com/halfcadence/i2cdevlib/commit/94677116c62aff6a2ce08d96522d3a8e89d12b4b)
pour que la lib fonctionne sur le wemos.

Besoin d'installer
[https://github.com/tzapu/WiFiManager](https://github.com/tzapu/WiFiManager )

 

### Oled Screen

[ThingPulse/esp8266-oled-ssd1306](https://ThingPulse/esp8266-oled-ssd1306)

NOTE:
J'ai rencontré un problème pour mettre à jour les id wifi. une solution: commenter les MPU, uploader, décommenter les MPU, uploader. Sinon, une exception
