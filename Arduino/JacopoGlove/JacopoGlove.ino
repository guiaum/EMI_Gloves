/*---------------------------------------------------------------------------------------------

  Open Sound Control (OSC) library for the ESP8266/ESP32

  Example for sending messages from the ESP8266/ESP32 to a remote computer
  The example is sending "hello, osc." to the address "/test".

  This example code is in the public domain.


  Wiring
  VL53L0X -> Wemos
  Vin -> 5V
  GND -> G
  SCL -> D1
  SDA -> D2

  MPU -> Wemos
  SDA -> D2
  SCL -> D1
  GND -> GND
  VCC -> 5V
  INT -> D8

  Oled
  VCC -> 5V
  GND -> G
  SCL -> D1
  SDA -> D2

  Button
  GND -> GND
  PIN2 -> D3

  --------------------------------------------------------------------------------------------- */
#define WITH_SCREEN
#define WITH_DIST
#define WITH_OSC
#define WITH_MPU
//#define GLOVE1
#define GLOVE2

#if defined(ESP8266)
#include <ESP8266WiFi.h>
#else
#include <WiFi.h>
#endif
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include "Id_Wifi.h"
// DISTANCE
#include "Adafruit_VL53L0X.h"

#ifdef WITH_SCREEN
// OLED Screen
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"
#endif


#ifdef WITH_MPU
// WITH HELP FROM https://forum.arduino.cc/index.php?topic=331084.0
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif
#define OUTPUT_READABLE_YAWPITCHROLL
#endif


WiFiUDP Udp;                                // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(192, 168, 10, 70);     // remote IP of your computer
const unsigned int outPort = 9999;          // remote port to receive OSC
const unsigned int localPort = 8888;        // local port to listen for OSC packets (actually not used for sending)

#ifdef WITH_DIST
Adafruit_VL53L0X lox = Adafruit_VL53L0X();
#endif

#ifdef WITH_SCREEN
// OLED Screen
// Initialize the OLED display using Wire library
SSD1306Wire  theDisplay(0x3c, D2, D1);
#endif

#ifdef WITH_MPU
MPU6050 mpu;

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
#ifdef OUTPUT_READABLE_EULER
float euler[3];         // [psi, theta, phi]    Euler angle container
#endif
#ifdef OUTPUT_READABLE_YAWPITCHROLL
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
#endif

#define INTERRUPT_PIN 15 // use pin 15 on ESP8266

const char DEVICE_NAME[] = "mpu6050";

#endif

// Var to be defined
#ifdef GLOVE1
const char whichGlove[] = "/glove1";
#endif
#ifdef GLOVE2
const char whichGlove[] = "/glove2";
#endif

const int buttonPin = 0;
int maxMeasure = 600;
int delayDist = 50;
long lastReadDist;

// Global variables sent
int sentMeasureDist;
float sentPitch;
float sentRoll;
int sentPositionPitch = 0; // between 1 and 6
int sentPositionRoll = 0; // between 1 and 6
int sentButton;
int offsetAngle = 20;

void setup() {
  Serial.begin(115200);

#ifdef WITH_MPU
  mpu_setup();
#endif

#ifdef WITH_SCREEN
  // OLED Screen
  // Initialising the UI will init the display too.
  theDisplay.init();
  theDisplay.flipScreenVertically();
  theDisplay.setFont(ArialMT_Plain_10);
#endif

#ifdef WITH_DIST
  Serial.println("Adafruit VL53L0X test");
  if (!lox.begin()) {
    Serial.println(F("Failed to boot VL53L0X"));
    while (1);
  }
#endif

  //Switch
  pinMode(buttonPin, INPUT);

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
#ifdef WITH_SCREEN
  theDisplay.drawString(0, 0, "Connecting to ");
  theDisplay.drawString(0, 10, ssid);
  theDisplay.display();
#endif
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
#ifdef ESP32
  Serial.println(localPort);
#else
  Serial.println(Udp.localPort());
#endif

}

boolean testAngle(float theAngle, float target)
{
  boolean testPassed = false;
  float topValue;
  float botValue;
  if (target != 0)
  {
    topValue = target + offsetAngle;
    botValue = target - offsetAngle;
    if (theAngle > botValue && theAngle < topValue) testPassed = true;
  }
  else
  {
    topValue = target + offsetAngle;
    botValue = 360 - offsetAngle;
    if (theAngle > botValue && theAngle < 360 || theAngle > 0 && theAngle < topValue ) testPassed = true;
  }

  return testPassed;
}

void loop() {

#ifdef WITH_MPU
  mpu_loop();

  if (testAngle(sentPitch, 0) ) sentPositionPitch = 1;
  else if (testAngle(sentPitch, 90) ) sentPositionPitch = 2;
  else if (testAngle(sentPitch, 180) ) sentPositionPitch = 3;
  else if (testAngle(sentPitch, 270) ) sentPositionPitch = 4;

  if (testAngle(sentRoll, 0) ) sentPositionRoll = 1;
  else if (testAngle(sentRoll, 90) ) sentPositionRoll = 2;
  else if (testAngle(sentRoll, 180) ) sentPositionRoll = 3;
  else if (testAngle(sentRoll, 270) ) sentPositionRoll = 4;


#endif


#ifdef WITH_DIST
  if (millis() - lastReadDist > delayDist)
  {
    //DISTANCE
    VL53L0X_RangingMeasurementData_t measure;
    //Serial.print("Reading a measurement... ");
    lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!
    if (measure.RangeStatus != 4 || measure.RangeMilliMeter<maxMeasure) {  // phase failures have incorrect data
      //Serial.print("Distance (mm): "); Serial.println(measure.RangeMilliMeter);
      sentMeasureDist = measure.RangeMilliMeter;
    } else {
      //Serial.println(" out of range ");
      sentMeasureDist = maxMeasure;
    }
    lastReadDist = millis();
  }
#endif

  sentButton = digitalRead(buttonPin);
#ifdef WITH_OSC
  // Send OSC
  sendOSC(sentMeasureDist, sentPositionPitch, sentPositionRoll, sentButton);
#endif

#ifdef WITH_SCREEN
  //OLED Screen
  displayInfo(sentMeasureDist, sentPitch, sentRoll, sentPositionPitch, sentPositionRoll, sentButton);
#endif

}

#ifdef WITH_SCREEN
// OLED
void displayInfo(int VL_Measure, float pitch, float roll, int PositionPitch, int PositionRoll, int Button)
{
  theDisplay.clear();

  String myIP = "IP: " + WiFi.localIP().toString();
  theDisplay.drawString(0, 0, myIP);

  String myDistMeasure = "Dist: " + String(VL_Measure);
  theDisplay.drawString(0, 15, myDistMeasure);

  String myOrientation = "Accel: " + String(pitch) + " " + String(roll);
  theDisplay.drawString(0, 25, myOrientation);

  String myPosition = "Pos: " + String(PositionPitch)+ " " +String(PositionRoll);
  theDisplay.drawString(0, 35, myPosition);

  String myButton = "Button: " + String(Button);
  theDisplay.drawString(0, 45, myButton);

  // write the buffer to the display
  theDisplay.display();
}
#endif

#ifdef WITH_OSC
//OSC
void sendOSC(int measure, int thePositionPitch, int thePositionRoll, int theButton)
{
  OSCMessage msg(whichGlove);
  msg.add(measure);
  msg.add(thePositionPitch);
  msg.add(thePositionRoll);
  msg.add(theButton);
  Udp.beginPacket(outIp, outPort);
  msg.send(Udp);
  Udp.endPacket();
  msg.empty();
}

#endif


#ifdef WITH_MPU
// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
  mpuInterrupt = true;
}

void mpu_setup()
{
  // join I2C bus (I2Cdev library doesn't do this automatically)
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
  Wire.begin();
  Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
  Fastwire::setup(400, true);
#endif

  // initialize device
  Serial.println(F("Initializing I2C devices..."));
  mpu.initialize();
  pinMode(INTERRUPT_PIN, INPUT);

  // verify connection
  Serial.println(F("Testing device connections..."));
  Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

  // load and configure the DMP
  Serial.println(F("Initializing DMP..."));
  devStatus = mpu.dmpInitialize();
  /*
    // supply your own gyro offsets here, scaled for min sensitivity
    mpu.setXGyroOffset(220);
    mpu.setYGyroOffset(76);
    mpu.setZGyroOffset(-85);
    mpu.setZAccelOffset(1788); // 1688 factory default for my test chip
  */

 #ifdef GLOVE1
  mpu.setXAccelOffset(-996);
  mpu.setYAccelOffset(696);
  mpu.setZAccelOffset(1117);
  mpu.setXGyroOffset(34);
  mpu.setYGyroOffset(-32);
  mpu.setZGyroOffset(-11);
#endif

 #ifdef GLOVE2
  mpu.setXAccelOffset(-836);
  mpu.setYAccelOffset(2440);
  mpu.setZAccelOffset(672);
  mpu.setXGyroOffset(77);
  mpu.setYGyroOffset(-22);
  mpu.setZGyroOffset(-17);
#endif

  // make sure it worked (returns 0 if so)
  if (devStatus == 0) {
    // turn on the DMP, now that it's ready
    Serial.println(F("Enabling DMP..."));
    mpu.setDMPEnabled(true);

    // enable Arduino interrupt detection
    Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;

    // get expected DMP packet size for later comparison
    packetSize = mpu.dmpGetFIFOPacketSize();
  } else {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
  }
}

void mpu_loop()
{
  // if programming failed, don't try to do anything
  if (!dmpReady) return;

  // wait for MPU interrupt or extra packet(s) available
  if (!mpuInterrupt && fifoCount < packetSize) return;

  // reset interrupt flag and get INT_STATUS byte
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();

  // get current FIFO count
  fifoCount = mpu.getFIFOCount();

  // check for overflow (this should never happen unless our code is too inefficient)
  if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
    // reset so we can continue cleanly
    mpu.resetFIFO();
    Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
  } else if (mpuIntStatus & 0x02) {
    // wait for correct available data length, should be a VERY short wait
    while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

    // read a packet from FIFO
    mpu.getFIFOBytes(fifoBuffer, packetSize);

    // track FIFO count here in case there is > 1 packet available
    // (this lets us immediately read more without waiting for an interrupt)
    fifoCount -= packetSize;

#ifdef OUTPUT_READABLE_QUATERNION
    // display quaternion values in easy matrix form: w x y z
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    Serial.print("quat\t");
    Serial.print(q.w);
    Serial.print("\t");
    Serial.print(q.x);
    Serial.print("\t");
    Serial.print(q.y);
    Serial.print("\t");
    Serial.println(q.z);

    sentPitch = q.x;
    sentRoll = q.y;

#endif

#ifdef OUTPUT_TEAPOT_OSC
#ifndef OUTPUT_READABLE_QUATERNION
    // display quaternion values in easy matrix form: w x y z
    mpu.dmpGetQuaternion(&q, fifoBuffer);
#endif
    // Send OSC message
    OSCMessage msg("/imuquat");
    msg.add((float)q.w);
    msg.add((float)q.x);
    msg.add((float)q.y);
    msg.add((float)q.z);

    Udp.beginPacket(outIp, outPort);
    msg.send(Udp);
    Udp.endPacket();

    msg.empty();
#endif

#ifdef OUTPUT_READABLE_EULER
    // display Euler angles in degrees
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetEuler(euler, &q);
    Serial.print("euler\t");
    Serial.print(euler[0] * 180 / M_PI);
    Serial.print("\t");
    Serial.print(euler[1] * 180 / M_PI);
    Serial.print("\t");
    Serial.println(euler[2] * 180 / M_PI);

    sentPitch = euler[1] * 180 / M_PI;
    sentRoll = euler[2] * 180 / M_PI;

#endif

#ifdef OUTPUT_READABLE_YAWPITCHROLL
    // display Euler angles in degrees
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
    Serial.print("ypr\t");
    Serial.print(ypr[0] * 180 / M_PI);
    Serial.print("\t");
    Serial.print(ypr[1] * 180 / M_PI);
    Serial.print("\t");
    Serial.println(ypr[2] * 180 / M_PI);
    sentPitch = ypr[1] * 180 / M_PI;
    sentRoll = ypr[2] * 180 / M_PI;
#endif

#ifdef OUTPUT_READABLE_REALACCEL
    // display real acceleration, adjusted to remove gravity
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetAccel(&aa, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
    Serial.print("areal\t");
    Serial.print(aaReal.x);
    Serial.print("\t");
    Serial.print(aaReal.y);
    Serial.print("\t");
    Serial.println(aaReal.z);
#endif

#ifdef OUTPUT_READABLE_WORLDACCEL
    // display initial world-frame acceleration, adjusted to remove gravity
    // and rotated based on known orientation from quaternion
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetAccel(&aa, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
    mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);
    Serial.print("aworld\t");
    Serial.print(aaWorld.x);
    Serial.print("\t");
    Serial.print(aaWorld.y);
    Serial.print("\t");
    Serial.println(aaWorld.z);
#endif
  }
}

#endif
