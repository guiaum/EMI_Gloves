import oscP5.*;
import netP5.*;

OscP5 oscP5;

int distGlove1;
int posGlove11;
int posGlove12;
int buttonGlove1;

int distGlove2;
int posGlove21;
int posGlove22;
int buttonGlove2;

void setup() {
    size(230, 400);

    /* start oscP5, listening for incoming messages at port 9999 */
    oscP5 = new OscP5(this, 9999);

    oscP5.plug(this, "glove1", "/glove1");
    oscP5.plug(this, "glove2", "/glove2");
}

public void glove1(int measure, int thePosition1, int thePosition2, int theButton) {
  println("Glove1", measure, thePosition1, thePosition2, theButton);
  
  distGlove1 = measure;
  posGlove11 = thePosition1;
  posGlove12 = thePosition2;
  buttonGlove1 = theButton;
  
  
}

public void glove2(int measure, int thePosition1, int thePosition2, int theButton) {
  println("Glove2", measure, thePosition1, thePosition2, theButton);
  
  distGlove2 = measure;
  posGlove21 = thePosition1;
  posGlove22 = thePosition2;
  buttonGlove2 = theButton;
  
  
}



void draw() {
    background(0);
    
    //GLOVE1
    //Dist
    fill(70,128,124);
    rect(20,50,15,min(300,distGlove1/2));
    
    //POS 1
    noFill();
    for (int i = 0; i<4; i++)
    {
      stroke(70,128,124);
      if (i+1 == posGlove11)
      {
        fill(70,128,124);
      } else {
        noFill();
      }
      rect ( 40, 50 + (i*20), 15, 15); 
    }
    
    //POS 2
    noFill();
    for (int i = 0; i<4; i++)
    {
      stroke(70,128,124);
      if (i+1 == posGlove12)
      {
        fill(70,128,124);
      } else {
        noFill();
      }
      rect ( 60, 50 + (i*20), 15, 15); 
    }
    
    //Button
    noFill();
    if (buttonGlove1 == 0)fill(70,128,124);
    rect ( 80, 50, 15, 15); 
    
    //GLOVE2
    //Dist
    fill(70,128,124);
    rect(20+80,50,15,min(300,distGlove2/2));
    
    //POS 1
    noFill();
    for (int i = 0; i<4; i++)
    {
      stroke(70,128,124);
      if (i+1 == posGlove21)
      {
        fill(70,128,124);
      } else {
        noFill();
      }
      rect ( 40+80, 50 + (i*20), 15, 15); 
    }
    
    //POS 2
    noFill();
    for (int i = 0; i<4; i++)
    {
      stroke(70,128,124);
      if (i+1 == posGlove22)
      {
        fill(70,128,124);
      } else {
        noFill();
      }
      rect ( 60+80, 50 + (i*20), 15, 15); 
    }
    
    //Button
    noFill();
    if (buttonGlove2 == 0)fill(70,128,124);
    rect ( 80+80, 50, 15, 15); 
}